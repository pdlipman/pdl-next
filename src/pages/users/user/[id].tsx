import { useRouter } from 'next/router';

export async function getServerSideProps() {
  const userName = 'userName';

  return {
    props: {
      userName,
    },
  };
}

type UserProps = {
  userName: string;
};

const User = ({ userName }: UserProps) => {
  const router = useRouter();
  const { id } = router.query;

  return (
    <>
      <h2>{`User: ${id} Name: ${userName}`}</h2>
    </>
  );
};

export default User;
