import { useEffect, useState } from 'react';

/**
 * useDebounce hook -
 *
 * @param value - value to watch
 * @param delay - time to wait before setting debounced value (in milliseconds)
 */
const useDebounce = <T>(value: T, delay = 2000) => {
  const [debouncedValue, setDebouncedValue] = useState<T>(value);

  useEffect(() => {
    const timeout = setTimeout(() => {
      setDebouncedValue(value);
    }, delay);

    return () => clearTimeout(timeout);
  }, [value, delay]);

  return debouncedValue;
};

export { useDebounce };
