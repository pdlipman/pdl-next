import styled from 'styled-components';

import { Input } from '@elements/input';

const StyledHeader = styled.div`
  background-color: lightgrey;
  position: sticky;
  top: 0;
  display: flex;
`;

const Header = () => {
  return (
    <StyledHeader>
      <Input placeholder={'Filter...'} />
    </StyledHeader>
  );
};

export { Header };
