import { useContext, useEffect, useState } from 'react';
import styled from 'styled-components';

import { filterContext } from '@context/filter';
import { useDebounce } from '@hooks/use-debounce';

const StyledInput = styled.input`
  font-size: 1em;
  border: 2px solid palevioletred;
  border-radius: 3px;
  margin: 1em;
  padding: 1em;
  &:focus {
    outline: none;
    border-color: transparent;
    box-shadow: 0 0 0 3px black;
  }
`;

type InputProps = {
  placeholder?: string;
};

const Input = ({ placeholder = '' }: InputProps) => {
  const [value, setValue] = useState('');
  const debouncedValue = useDebounce(value, 500);
  const { setFilterValue } = useContext(filterContext);
  const handleChange = (event: React.FormEvent<HTMLInputElement>) => {
    setValue(event.currentTarget.value);
  };

  useEffect(() => {
    setFilterValue(debouncedValue);
  }, [debouncedValue, setFilterValue]);

  return (
    <StyledInput
      placeholder={placeholder}
      onChange={handleChange}
      value={value}
    />
  );
};

export { Input };
