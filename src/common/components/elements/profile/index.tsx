import { useUser } from '@auth0/nextjs-auth0';

const Profile = () => {
  const { user, error, isLoading } = useUser();
  console.log('phil test user begin');
  console.log(user);
  console.log('phil test user end');

  if (isLoading) return <div>Loading...</div>;
  if (error) return <div>{error.message}</div>;

  return (
    <div>
      <h2>{user?.name}</h2>
      <p>{user?.email}</p>
    </div>
  );
};

export { Profile };
