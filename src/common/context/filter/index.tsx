import { createContext } from 'react';

const filterContext = createContext({
  filterValue: '',
  setFilterValue: (_value: string) => {},
});

export { filterContext };
